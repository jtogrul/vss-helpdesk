<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppHelper', 'View/Helper');

class MenuHelper extends AppHelper {

    public $helpers = array('Html');

    public $all_items = array(
        'admin' => array(
            array(
                'title' => 'Açıq sorğular',
                'icon' => 'support',
                'url' => array('controller' => 'tickets', 'action' => 'index')
            ),
            array(
                'title' => 'Arxiv',
                'icon' => 'archive',
                'url' => array('controller' => 'tickets', 'action' => 'index', '?' => array('archive' => 1))
            ),
            array(
                'title' => 'Müştərilər',
                'icon' => 'user',
                'url' => array('controller' => 'customers', 'action' => 'index')
            ),
            array(
                'title' => 'Statistika',
                'icon' => 'line-chart',
                'url' => array('controller' => 'stats', 'action' => 'index')
            ),
            array(
                'title' => 'Avadanlıqlar',
                'icon' => 'puzzle-piece',
                'url' => array('controller' => 'equipment', 'action' => 'index')
            ),
            array(
                'title' => 'Şirkətlər',
                'icon' => 'building-o',
                'url' => array('controller' => 'companies', 'action' => 'index')
            ),
            array(
                'wrapper' => true,
                'name' => 'others',
                'title' => 'Digər',
                'items' => array(
                    array(
                        'title' => 'Statuslar',
                        'icon' => 'flag',
                        'url' => array('controller' => 'statuses', 'action' => 'index')
                    ),
                    array(
                        'title' => 'Müraciət tipləri',
                        'icon' => 'file-text',
                        'url' => array('controller' => 'request_types', 'action' => 'index')
                    ),
                    array(
                        'title' => 'Giriş detalları',
                        'icon' => 'file-text',
                        'url' => array('controller' => 'users', 'action' => 'index')
                    ),
                    array(
                        'title' => 'Adminlər',
                        'icon' => 'file-text',
                        'url' => array('controller' => 'admins', 'action' => 'index')
                    )
                )
            )
        ),
        'moderator' => array(
            array(
                'title' => 'Açıq sorğular',
                'icon' => 'support',
                'url' => array('controller' => 'tickets', 'action' => 'index')
            ),
            array(
                'title' => 'Arxivlənmiş sorğular',
                'icon' => 'archive',
                'url' => array('controller' => 'tickets', 'action' => 'index', '?' => array('archive' => 1))
            )
        ),
        'customer' => array(
            array(
                'title' => 'Açıq sorğular',
                'icon' => 'support',
                'url' => array('controller' => 'tickets', 'action' => 'index')
            ),
            array(
                'title' => 'Arxivlənmiş sorğuıar',
                'icon' => 'archive',
                'url' => array('controller' => 'tickets', 'action' => 'index', '?' => array('archive' => 1))
            ),
            array(
                'title' => 'Yeni Sorğu yarat',
                'icon' => 'plus',
                'url' => array('controller' => 'tickets', 'action' => 'add')
            )
        )
    );

    public function makeList($role) {
        // TODO
        $items = $this->all_items[$role];

        echo '<ul class="nav navbar-nav side-nav">';

        foreach($items as $item) {

            if (isset($item['wrapper']) && $item['wrapper']) {
                $this->createItemsWrapper($item);
            } else {
                $this->createSingleItem($item);
            }

        }

        echo '</ul>';
    }

    public function createSingleItem($item) {
        /**
         * <li><a href="index.html"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a></li>
         */

        echo '<li>';
        echo $this->Html->link('<i class="fa fa-fw fa-'.$item['icon'].'"></i> '.$item['title'], $item['url'], array('escape' => false));
        echo '</li>';

    }

    public function createItemsWrapper($wrapper) {
        echo '<li>';

        echo '<a href="javascript:;" data-toggle="collapse" data-target="#'.$wrapper['name'].'">'
            .'<i class="fa fa-fw fa-arrows-v"></i> '.$wrapper['title'].' <i class="fa fa-fw fa-caret-down"></i>'
            .'</a>';

        echo '<ul id="'.$wrapper['name'].'" class="collapse">';

        foreach($wrapper['items'] as $item) {
            $this->createSingleItem($item);
        }

        echo '</ul>';

        echo '</li>';
    }

}
