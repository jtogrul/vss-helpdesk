<div class="users form">
<?php echo $this->Form->create('Customer'); ?>
	<fieldset>
		<legend><?php echo __('Registration'); ?></legend>
	<?php
		echo $this->Form->input('Customer.first_name');
		echo $this->Form->input('Customer.last_name');
		echo $this->Form->input('User.username');
		echo $this->Form->input('User.password');
	?>
	<?php echo $this->Recaptcha->display(array('div' => array('class' => 'col-lg-offset-2'))); ?>
	</fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>