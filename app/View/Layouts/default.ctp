<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		Helpdesk Admin:
		<?php echo $this->fetch('title'); ?>
	</title>
	<meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="description" content=""/>
        <meta name="author" content=""/>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('sb-admin');
		echo $this->Html->css('font-awesome/css/font-awesome.min');
		echo $this->Html->css('helpdesk.general');
        echo $this->Html->css('helpdesk.admin');

		echo $this->Html->script('jquery-1.11.0');
		echo $this->Html->script('bootstrap.min');
		echo $this->Html->script('toolbar');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        var user_role_general = "<?php echo $user_role_general; ?>";
        var base_url = "<?php echo $this->Html->url('/', true); ?>";
    </script>
</head>
<body>

	<div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <?php echo $this->Html->link('Helpdesk', '/', array('class' => 'navbar-brand')); ?>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown" id="messages-dropdown-toolbar">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <span class="badge alert-info"><?php echo $messages_count; ?></span> <b class="caret"></b></a>
                        <ul class="dropdown-menu message-dropdown" id="messages-box-toolbar">
                            <li class="message-footer">
                                <a href="#">Loading...</a>
                            </li>
                        </ul>
                    </li>
                    <!--<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                        <ul class="dropdown-menu alert-dropdown">
                            <li>
                                <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                            </li>
                            <li>
                                <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">View All</a>
                            </li>
                        </ul>
                    </li>-->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $user_full_name; ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <?php $logout_url = $this->Html->url(array('controller' => 'users', 'action' => 'logout', 'admin' => false)); ?>
                                <a href="<?php echo $logout_url; ?>"><i class="fa fa-fw fa-power-off"></i> <?php echo __('Log Out'); ?></a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <?php if(isset($role)): ?>

                    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse">

                        <?php echo $this->Menu->makeList($role); ?>

                        <script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>
                        <div id="SkypeButton_Call_jtogrul_1">
                            <script type="text/javascript">
                                Skype.ui({
                                    "name": "dropdown",
                                    "element": "SkypeButton_Call_jtogrul_1",
                                    "participants": ["jtogrul"]
                                });
                            </script>
                        </div>

                    </div>
                    <!-- /.navbar-collapse -->
                <?php endif; ?>
            </nav>

            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                <?php echo $title_for_layout; ?>
                            </h1>
                        </div>
                    </div>
                    <!-- /.row -->

                    <?php echo $this->Session->flash(); ?>
                    <?php echo $this->fetch('content'); ?>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

    <!-- BEGIN JIVOSITE CODE {literal} -->
    <script type='text/javascript'>
        (function(){ var widget_id = '0X3JUus6lw';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();
    </script>
    <!-- {/literal} END JIVOSITE CODE -->
</body>
</html>
