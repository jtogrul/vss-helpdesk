<div class="equipment view col-sm-8 well">
<h2><?php echo __('Equipment'); ?></h2>
	<dl class="dl-horizontal">
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($equipment['Equipment']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($equipment['Equipment']['name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions col-sm-4">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Equipment'), array('action' => 'edit', $equipment['Equipment']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Equipment'), array('action' => 'delete', $equipment['Equipment']['id']), array(), __('Are you sure you want to delete # %s?', $equipment['Equipment']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Equipment'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Equipment'), array('action' => 'add')); ?> </li>
	</ul>
</div>