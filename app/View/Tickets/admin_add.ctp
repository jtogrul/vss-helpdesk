<div class="tickets form col-sm-8 well">
<?php echo $this->Form->create('Ticket'); ?>
	<fieldset>
		<legend><?php echo __('Add Ticket'); ?></legend>
	<?php
		echo $this->Form->input('customer_id');
		echo $this->Form->input('equipment_id');
		echo $this->Form->input('status_id');
		echo $this->Form->input('request_type_id');
		echo $this->Form->input('subject');
		echo $this->Form->input('viewed');
		echo $this->Form->input('open');
		echo $this->Form->input('satisfied');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions col-sm-4">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Tickets'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
