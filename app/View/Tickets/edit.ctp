<div class="tickets form">
<?php echo $this->Form->create('Ticket'); ?>
	<fieldset>
		<legend><?php echo __('Edit Ticket'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('customer_id');
		echo $this->Form->input('equipment_id');
		echo $this->Form->input('status_id');
		echo $this->Form->input('request_type_id');
		echo $this->Form->input('subject');
		echo $this->Form->input('viewed');
		echo $this->Form->input('open');
		echo $this->Form->input('satisfied');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>