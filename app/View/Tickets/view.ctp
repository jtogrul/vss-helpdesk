<div class="tickets view">
<h2><?php echo __('Ticket'); ?></h2>

<div class="row">

    <div class="col-sm-8">
        <div class="panel panel-primary">
            <div class="panel-heading"><?php echo __('Details'); ?></div>
            <div class="panel-body">
                <dl class="dl-horizontal">
                    <dt><?php echo __('Id'); ?></dt>
                    <dd>
                        <?php echo h($ticket['Ticket']['id']); ?>
                        &nbsp;
                    </dd>
                    <dt><?php echo __('Customer'); ?></dt>
                    <dd>
                        <?php echo $this->Html->link($ticket['Customer']['id'], array('controller' => 'customers', 'action' => 'view', $ticket['Customer']['id'])); ?>
                        &nbsp;
                    </dd>
                    <dt><?php echo __('Equipment'); ?></dt>
                    <dd>
                        <?php echo $this->Html->link($ticket['Equipment']['name'], array('controller' => 'equipment', 'action' => 'view', $ticket['Equipment']['id'])); ?>
                        &nbsp;
                    </dd>
                    <dt><?php echo __('Status'); ?></dt>
                    <dd>
                        <?php echo $this->Html->link($ticket['Status']['name'], array('controller' => 'statuses', 'action' => 'view', $ticket['Status']['id'])); ?>
                        &nbsp;
                    </dd>
                    <dt><?php echo __('Request Type'); ?></dt>
                    <dd>
                        <?php echo $this->Html->link($ticket['RequestType']['name'], array('controller' => 'request_types', 'action' => 'view', $ticket['RequestType']['id'])); ?>
                        &nbsp;
                    </dd>
                    <dt><?php echo __('Subject'); ?></dt>
                    <dd>
                        <?php echo h($ticket['Ticket']['subject']); ?>
                        &nbsp;
                    </dd>
                    <dt><?php echo __('Viewed'); ?></dt>
                    <dd>
                        <?php echo h($ticket['Ticket']['viewed']); ?>
                        &nbsp;
                    </dd>
                    <dt><?php echo __('Open'); ?></dt>
                    <dd>
                        <?php echo h($ticket['Ticket']['open']); ?>
                        &nbsp;
                    </dd>
                    <dt><?php echo __('Satisfied'); ?></dt>
                    <dd>
                        <?php echo h($ticket['Ticket']['satisfied']); ?>
                        &nbsp;
                    </dd>
                    <dt><?php echo __('Created'); ?></dt>
                    <dd>
                        <?php echo h($ticket['Ticket']['created']); ?>
                        &nbsp;
                    </dd>
                    <dt><?php echo __('Modified'); ?></dt>
                    <dd>
                        <?php echo h($ticket['Ticket']['modified']); ?>
                        &nbsp;
                    </dd>
                </dl>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="panel panel-warning">
            <div class="panel-heading"><?php echo __('Actions'); ?></div>
            <div class="panel-body">
                <?php echo $this->Form->create('Ticket', array('action' => 'open_set')); ?>
                    <fieldset>
                        <?php echo $this->Form->input('id', array('default' => $ticket['Ticket']['id'])); ?>
                        <?php echo $this->Form->input('open', array('default' => $ticket['Ticket']['open'])); ?>
                    </fieldset>
                <?php echo $this->Form->end('Set'); ?>
                <hr />
                <?php echo $this->Form->create('Ticket', array('action' => 'satisfied_set')); ?>
                    <fieldset>
                        <?php echo $this->Form->input('id', array('default' => $ticket['Ticket']['id'])); ?>
                        <?php echo $this->Form->input('satisfied', array('default' => $ticket['Ticket']['satisfied'])); ?>
                    </fieldset>
                <?php echo $this->Form->end('Set'); ?>
            </div>
        </div>
    </div>

</div>


</div>
<div class="row">
	<h3><?php echo __('Messages'); ?></h3>
	<?php foreach ($ticket['Message'] as $message): ?>
			<?php echo $this->element('message', array('message' => $message)); ?>
	<?php endforeach; ?>

    <div class="messages form">
        <?php echo $this->Form->create('Message', array('controller' => 'Toolbar', 'action' => 'add')); ?>
            <fieldset>
                <legend><?php echo __('Reply'); ?></legend>
            <?php
                echo $this->Form->hidden('ticket_id', array('value' => $ticket['Ticket']['id']));
                echo $this->Form->input('body', array('label' => false));
            ?>
            </fieldset>
        <?php echo $this->Form->end(__('Submit'), array('class' => 'btn btn-primary')); ?>
    </div>

</div>
