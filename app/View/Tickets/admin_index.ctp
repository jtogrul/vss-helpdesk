<div class="tickets index">
	<table cellpadding="0" cellspacing="0" class="table table-striped">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_id'); ?></th>
			<th><?php echo $this->Paginator->sort('equipment_id'); ?></th>
			<th><?php echo $this->Paginator->sort('status_id'); ?></th>
			<th><?php echo $this->Paginator->sort('request_type_id'); ?></th>
			<th><?php echo $this->Paginator->sort('subject'); ?></th>
			<th><?php echo $this->Paginator->sort('viewed'); ?></th>
			<th><?php echo $this->Paginator->sort('satisfied'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($tickets as $ticket): ?>
	<tr>
		<td><?php echo h($ticket['Ticket']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($ticket['Customer']['full_name'], array('controller' => 'customers', 'action' => 'view', $ticket['Customer']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($ticket['Equipment']['name'], array('controller' => 'equipment', 'action' => 'view', $ticket['Equipment']['id'])); ?>
		</td>
		<td>
			<?php echo $ticket['Status']['name']; ?>
		</td>
		<td>
			<?php echo $ticket['RequestType']['name']; ?>
		</td>
		<td><?php echo h($ticket['Ticket']['subject']); ?>&nbsp;</td>
		<td><?php echo h($ticket['Ticket']['viewed']); ?>&nbsp;</td>
		<td><?php echo h($ticket['Ticket']['satisfied']); ?>&nbsp;</td>
		<td><?php echo h($ticket['Ticket']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $ticket['Ticket']['id']), array('class' => 'btn btn-primary')); ?>
        </td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>

	<?php foreach ($tickets as $ticket): ?>
    	<?php /*echo $this->element('ticket', array('ticket' =>  $ticket));*/ ?>
    <?php endforeach; ?>

	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

    <ul class="pagination">
    <?php
    echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
    echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
    echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
    ?>
    </ul>

</div>
