<?php if(! empty($messages)): ?>
<?php foreach ($messages as $message): ?>
<li class="message-preview">

    <?php
        $ticket_url = $this->Html->url(array('controller' => 'tickets', 'action' => 'view', $message['Message']['ticket_id']));
    ?>

    <a href="<?php echo $ticket_url; ?>">
        <div class="media">
            <div class="media-body">
                <h5 class="media-heading">
                    <strong><?php echo $message['Message']['full_name']; ?></strong>
                </h5>
                <p class="small text-muted"><i class="fa fa-clock-o"></i> <?php echo $this->Time->nice($message['Message']['created']); ?></p>
                <p><?php echo $message['Message']['body']; ?></p>
            </div>
        </div>
    </a>
</li>
<?php endforeach; ?>

<?php else: ?>
<li class="message-footer">
    <a href="#"><?php echo __('No new messages'); ?></a>
</li>
<?php endif; ?>