<div class="media message role-<?php echo $message['role']; ?>">
  <span class="pull-left">
    <?php $icon = $message['role'] === 'customer' ? 'fa-user' : 'fa-support'; ?>
    <i class="fa <?php echo $icon; ?> fa-4x"></i>
  </span>
  <div class="media-body">
    <h4 class="media-heading"><?php echo $message['full_name']; ?></h4>
    <p><?php echo $message['body']; ?></p>
    <p class="small text-muted">
        <i class="fa fa-clock-o"></i> <?php echo $this->Time->nice($message['created']); ?>
        <?php if($user_role_general != 'customer') {
            echo $this->Html->link(
                __('Edit message'),
                array('controller' => 'messages', 'action' => 'edit', $message['id']),
                array('class' => 'message-edit')
            );
        } ?>
    </p>
  </div>
</div>