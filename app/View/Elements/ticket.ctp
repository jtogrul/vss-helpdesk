<div class="sm-col-6">
    <a href="#">
        <div class="media ticket" href="#">
            <div class="media-body">
                <h4 class="media-heading"><?php echo $ticket['Ticket']['subject']; ?></h4>
                <p class="small text-muted"><i class="fa fa-clock-o"></i> <?php echo $this->Time->nice($ticket['Ticket']['created']); ?></p>
                <p><?php echo __('Customer'); ?>: <?php echo $ticket['Customer']['full_name']; ?></p>
                <p><?php echo __('Status'); ?>: <?php echo $ticket['Status']['name']; ?></p>
                <p><?php echo __('Equipment'); ?>: <?php echo $ticket['Equipment']['name']; ?></p>
                <p><?php echo __('Request type'); ?>: <?php echo $ticket['RequestType']['name']; ?></p>
            </div>
        </div>
    </a>
</div>