<div class="requestTypes view col-sm-8 well">
<h2><?php echo __('Request Type'); ?></h2>
	<dl class="dl-horizontal">
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($requestType['RequestType']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($requestType['RequestType']['name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions col-sm-4">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Request Type'), array('action' => 'edit', $requestType['RequestType']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Request Type'), array('action' => 'delete', $requestType['RequestType']['id']), array(), __('Are you sure you want to delete # %s?', $requestType['RequestType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Request Types'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Request Type'), array('action' => 'add')); ?> </li>
	</ul>
</div>