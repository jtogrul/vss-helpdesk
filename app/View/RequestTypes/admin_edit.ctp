<div class="requestTypes form col-sm-8 well">
<?php echo $this->Form->create('RequestType'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Request Type'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions col-sm-4">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('RequestType.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('RequestType.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Request Types'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Tickets'), array('controller' => 'tickets', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ticket'), array('controller' => 'tickets', 'action' => 'add')); ?> </li>
	</ul>
</div>
