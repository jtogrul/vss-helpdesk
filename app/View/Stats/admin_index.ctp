<?php echo $this->Html->script('plugins/morris/morris.min'); ?>
<?php echo $this->Html->script('plugins/morris/raphael.min'); ?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-green">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> <?php echo __('Equipment tickets by month'); ?></h3>
            </div>
            <div class="panel-body">
                <div id="equipment-tickets-by-month"></div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-green">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> <?php echo __('Answers by moderators'); ?></h3>
                <?php echo $this->Form->input('admin', array(
                        'options' => $admins,
                        'empty' => __('Everyone')
                    ),
                    array('div' => array('class' => 'inline') )
                ); ?>
            </div>
            <div class="panel-body">
                <div id="answers-by-moderators"></div>
            </div>
        </div>
    </div>
</div>


<script>

    $(document).ready(function(){

        var loadEquipmentTicketsByMonths = function() {
            $.getJSON("<?php echo $this->Html->url(array('action' => 'equipments_by_month_ajax')); ?>", function( stats ) {

                Morris.Area({
                    element: 'equipment-tickets-by-month',
                    data: stats.data,
                    xkey: 'm',
                    ykeys: stats.keys,
                    labels: stats.labels
                });

            });
        }

        var loadAnswersByModerators = function(user_id) {
            $.getJSON("<?php echo $this->Html->url(array('action' => 'answers_by_mods_ajax', '?' => array('user_id' => user_id))); ?>", function( stats ) {

                Morris.Area({
                    element: 'answers-by-moderators',
                    data: stats.data,
                    xkey: 'm',
                    ykeys: stats.keys,
                    labels: stats.labels
                });

            });
        }

        loadEquipmentTicketsByMonths();
        loadAnswersByModerators(-1);

        // TODO loadAnswersByModerators(user_id) on user set btn click

    });

</script>