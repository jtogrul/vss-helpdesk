<div class="row">
    <div class="customers view col-sm-8 well">
        <h2><?php echo __('Customer'); ?></h2>
        <dl class="dl-horizontal">
            <dt><?php echo __('Id'); ?></dt>
            <dd>
                <?php echo h($customer['Customer']['id']); ?>
                &nbsp;
            </dd>
            <dt><?php echo __('User'); ?></dt>
            <dd>
                <?php echo $this->Html->link($customer['User']['id'], array('controller' => 'users', 'action' => 'view', $customer['User']['id'])); ?>
                &nbsp;
            </dd>
            <dt><?php echo __('Company'); ?></dt>
            <dd>
                <?php echo $this->Html->link($customer['Company']['name'], array('controller' => 'companies', 'action' => 'view', $customer['Company']['id'])); ?>
                &nbsp;
            </dd>
            <dt><?php echo __('First Name'); ?></dt>
            <dd>
                <?php echo h($customer['Customer']['first_name']); ?>
                &nbsp;
            </dd>
            <dt><?php echo __('Last Name'); ?></dt>
            <dd>
                <?php echo h($customer['Customer']['last_name']); ?>
                &nbsp;
            </dd>
            <dt><?php echo __('Work position'); ?></dt>
            <dd>
                <?php echo h($customer['Customer']['position']); ?>
                &nbsp;
            </dd>
            <dt><?php echo __('Room number'); ?></dt>
            <dd>
                <?php echo h($customer['Customer']['room']); ?>
                &nbsp;
            </dd>
            <dt><?php echo __('Phone'); ?></dt>
            <dd>
                <?php echo h($customer['Customer']['phone']); ?>
                &nbsp;
            </dd>
            <dt><?php echo __('Email'); ?></dt>
            <dd>
                <?php echo h($customer['Customer']['email']); ?>
                &nbsp;
            </dd>
            <dt><?php echo __('Created'); ?></dt>
            <dd>
                <?php echo h($customer['Customer']['created']); ?>
                &nbsp;
            </dd>
            <dt><?php echo __('Modified'); ?></dt>
            <dd>
                <?php echo h($customer['Customer']['modified']); ?>
                &nbsp;
            </dd>
        </dl>
    </div>
    <div class="actions col-sm-4">
        <h3><?php echo __('Actions'); ?></h3>
        <ul>
            <li><?php echo $this->Html->link(__('Edit Customer'), array('action' => 'edit', $customer['Customer']['id'])); ?> </li>
            <li><?php echo $this->Form->postLink(__('Delete Customer'), array('action' => 'delete', $customer['Customer']['id']), array(), __('Are you sure you want to delete # %s?', $customer['Customer']['id'])); ?> </li>
            <li><?php echo $this->Html->link(__('List Customers'), array('action' => 'index')); ?> </li>
            <li><?php echo $this->Html->link(__('New Customer'), array('action' => 'add')); ?> </li>
        </ul>
    </div>
</div>
<div class="related clearfix">
	<h3><?php echo __('Related Tickets'); ?></h3>
	<?php if (!empty($customer['Ticket'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Customer Id'); ?></th>
		<th><?php echo __('Equipment Id'); ?></th>
		<th><?php echo __('Status Id'); ?></th>
		<th><?php echo __('Request Type Id'); ?></th>
		<th><?php echo __('Subject'); ?></th>
		<th><?php echo __('Viewed'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($customer['Ticket'] as $ticket): ?>
		<tr>
			<td><?php echo $ticket['id']; ?></td>
			<td><?php echo $ticket['customer_id']; ?></td>
			<td><?php echo $ticket['equipment_id']; ?></td>
			<td><?php echo $ticket['status_id']; ?></td>
			<td><?php echo $ticket['request_type_id']; ?></td>
			<td><?php echo $ticket['subject']; ?></td>
			<td><?php echo $ticket['viewed']; ?></td>
			<td><?php echo $ticket['created']; ?></td>
			<td><?php echo $ticket['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'tickets', 'action' => 'view', $ticket['id']), array('class' => 'btn btn-primary')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'tickets', 'action' => 'edit', $ticket['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'tickets', 'action' => 'delete', $ticket['id']), array(), __('Are you sure you want to delete # %s?', $ticket['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Ticket'), array('controller' => 'tickets', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
