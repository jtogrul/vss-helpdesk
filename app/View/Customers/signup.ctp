<div class="customers form">
<?php echo $this->Form->create('Customer'); ?>
	<fieldset>
		<legend><?php echo __('Add Customer'); ?></legend>
	<?php
	    echo $this->Form->input('first_name');
        echo $this->Form->input('last_name');
        echo $this->Form->input('company_id');
        echo $this->Form->input('position');
        echo $this->Form->input('room');
        echo $this->Form->input('phone');
        echo $this->Form->input('email');
		echo $this->Form->input('User.username');
		echo $this->Form->input('User.password');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
