<?php
App::uses('AppController', 'Controller');
/**
 * Messages Controller
 *
 * @property Message $Message
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class MessagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Toolbar');

    public $helpers = array(
        'Session',
        'Html' => array('className' => 'BoostCake.BoostCakeHtml'),
        'Form' => array('className' => 'BootstrapForm'),
        'Paginator' => array('className' => 'BoostCake.BoostCakePaginator'),
    );

    /**
     * unread messages notifications (for admin)
     *
     * @return json
     */
    public function admin_unread_ajax() {
        $this->request->allowMethod('ajax');
        $this->layout = false;

        $messages = $this->Message->find('all', array('conditions' => array(
            'Message.role' => 'customer',
            'Message.read' => 0
        )));

        $this->set(compact('messages'));
    }

    public function unread_ajax() {
        //$this->request->allowMethod('ajax');
        $this->layout = false;

        $messages = $this->Message->find('all', array('conditions' => array(
            'Message.role !=' => 'customer',
            'Ticket.customer_id' => $this->Session->read('Customer')['Customer']['id'],
            'Message.read' => 0
        )));

        $this->set(compact('messages'));
    }


    /**
     * add method
     *
     * @return void
     */
    public function add() {

        if ($this->request->is('post')) {

            $message = $this->request->data;

            if (!empty($message['Message']['ticket_id'])) {

                $this->loadModel('Customer');

                $customer = $this->Customer->find('first', array(
                    'conditions' => array('Customer.user_id' => $this->Auth->user('id'))
                ));

                $exists = $this->Message->Ticket->hasAny(array(
                    'Ticket.id' => $message['Message']['ticket_id'],
                    'Ticket.customer_id' => $customer['Customer']['id']
                ));

                if ($exists) {
                    $this->Message->create();

                    $message['Message']['user_id'] = $this->Auth->user('id');
                    $message['Message']['full_name'] = $customer['Customer']['full_name'];
                    $message['Message']['role'] = 'customer';

                    if ($this->Message->save($message)) {
                        $this->Session->setFlash(__('The message has been saved.'));
                    } else {
                        $this->Session->setFlash(__('The message could not be saved. Please, try again.'));
                    }
                }

            } else {
                $this->Session->setFlash(__('The message could not be saved. Please, try again.'));
            }

            return $this->redirect(array('controller' => 'tickets', 'action' => 'view', $message['Message']['ticket_id']));

        }
    }

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
            $message = $this->request->data;

            if (!empty($message['Message']['ticket_id'])) {

                $this->loadModel('Admin');

                $admin = $this->Admin->find('first', array(
                    'conditions' => array('Admin.user_id' => $this->Auth->user('id'))
                ));

                $this->Message->create();

                $message['Message']['user_id'] = $this->Auth->user('id');
                $message['Message']['full_name'] = $admin['Admin']['full_name'];
                $message['Message']['role'] = $this->Auth->user('role');

                if ($this->Message->save($message)) {
                    $this->Session->setFlash(__('The message has been saved.'));
                } else {
                    $this->Session->setFlash(__('The message could not be saved. Please, try again.'));
                }

            } else {
                $this->Session->setFlash(__('The message could not be saved. Please, try again.'));
            }

            return $this->redirect(array('controller' => 'tickets', 'action' => 'view', $message['Message']['ticket_id']));
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Message->exists($id)) {
			throw new NotFoundException(__('Invalid message'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Message->save($this->request->data)) {
				$this->Session->setFlash(__('The message has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The message could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Message.' . $this->Message->primaryKey => $id));
			$this->request->data = $this->Message->find('first', $options);
		}
		$tickets = $this->Message->Ticket->find('list');
		$this->set(compact('tickets'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Message->id = $id;
		if (!$this->Message->exists()) {
			throw new NotFoundException(__('Invalid message'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Message->delete()) {
			$this->Session->setFlash(__('The message has been deleted.'));
		} else {
			$this->Session->setFlash(__('The message could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('controller' => 'tikcets','action' => 'index'));
	}
}
