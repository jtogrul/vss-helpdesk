<?php
/**
 * Created by PhpStorm.
 * User: togrul
 * Date: 10/26/14
 * Time: 12:33 PM
 */

App::uses('Component', 'Controller');
class ToolbarComponent extends Component {

    public $components = array('Auth', 'Session');

    public function initialize(Controller $controller) {

        if ($this->Auth->loggedIn()) {
            $role = $this->Auth->user('role');
            $messageModel = ClassRegistry::init('Message');

            $options = array(
                'conditions' => array(
                    'Message.read' => 0
                )
            );

            if ($role === 'customer') {
                $options['conditions']['Message.role !='] = 'customer';
                $options['conditions']['Ticket.customer_id'] = $this->Session->read('Customer')['Customer']['id'];
            } else {
                $options['conditions']['Message.role'] = 'customer';
            }

            $count = $messageModel->find('count',$options);

            $role_general = $role === 'customer'? 'customer' : 'admin';

            $controller->set('messages_count', $count);
            $controller->set('user_role_general', $role_general);
            $controller->set('user_full_name', $this->Auth->user(ucfirst($role_general))['full_name']);
        }

    }



} 