<?php
/**
 * Created by PhpStorm.
 * User: togrul
 * Date: 10/21/14
 * Time: 6:10 PM
 */

App::uses('AppController', 'Controller');

class HomeController extends AppController {

    public function index() {
        return $this->redirect(array('controller' => 'users', 'action' => 'login', 'admin' => false));
    }

} 