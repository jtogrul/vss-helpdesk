<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Toolbar', 'Recaptcha.Recaptcha');

    public $helpers = array(
        'Session',
        'Html' => array('className' => 'BoostCake.BoostCakeHtml'),
        'Form' => array('className' => 'BootstrapForm'),
        'Paginator' => array('className' => 'BoostCake.BoostCakePaginator'),
    );

    function beforeFilter() {
        $this->Auth->allow('signup');
    }

    public function login() {

        $this->layout = 'guest';

        if ($this->request->is('post')) {

            if ($this->Auth->login()) {

                if ($this->Auth->user('role') == 'customer') {
                
                    $customer = $this->User->Customer->findByUserId($this->Auth->user('id'));
                    
                    $companyNull = empty($customer['Customer']['company_id']);
                
                    if ($companyNull) {
            	        $this->Session->setFlash('Sizin hesab hələ admin tərəfindən heç bir şirkət ilə əlaqələndirilməyib. Şirkət ilə əlaqə saxlayın');
            	        return $this->redirect($this->Auth->logout());
            	    }
                
                    $this->Session->write('Customer',  $customer);
                    return $this->redirect($this->Auth->loginRedirect);
                } else if ($this->Auth->user('role') == 'admin' || $this->Auth->user('role') == 'moderator') {
                    $this->Session->write('Admin',  $this->User->Admin->findByUserId($this->Auth->user('id')));
                    return $this->redirect(array('controller' => 'tickets', 'action' => 'index', 'admin' => true));
                }
            }
            $this->Session->setFlash(__('Invalid username or password, try again'));
        }

    }

    public function signup() {

        $this->layout = 'guest';

        if ($this->request->is('post')) {

            if (! $this->User->hasAny(array('User.username' => $this->request->data['User']['username']))) {

                if ($this->Recaptcha->verify()) {

                    if ($this->User->Customer->saveAssociated($this->request->data)) {
                        $this->Session->setFlash(__('The user has been saved.'));
                        return $this->redirect(array('action' => 'login'));
                    } else {
                        $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
                    }

                } else {
                    $this->Session->setFlash(__('Please write code in picture correctly'));
                }

            } else {
                $this->Session->setFlash(__('Customer with this username already exists'));
            }

        }

        $companies = $this->User->Customer->Company->find('list');
        $this->set(compact('companies'));

    }

    public function admin_login() {
        return $this->redirect(array('controller' => 'users', 'action' => 'login', 'admin' => false));
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.', 'alert-box', array('class' => 'alert-success')));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.', 'alert-box', array('class' => 'alert-error')));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$user = $this->User->find('first', $options);
			unset($user['User']['password']);
			$this->request->data = $user;
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
