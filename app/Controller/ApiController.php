<?php
App::uses('Controller', 'Controller');
/**
 * Api controller
 */
class ApiController extends Controller {

    public $uses = 'Ticket';

    public $customer;

    public $components = array(
        'RequestHandler',
        'Auth' => array(
            'authenticate' => array(
                'Basic' => array(
                    'passwordHasher' => 'Blowfish'
                )
            )
        ),
        'Paginator'
    );

    function beforeFilter() {
        parent::beforeFilter();
        //$this->loadModel('Ticket');
        $this->customer = $this->Ticket->Customer->find('first', array(
            'conditions' => array('Customer.user_id' => $this->Auth->user('id'))
        ));
    }

    public function check_login() {

        $customer = $this->Ticket->Customer->find('first', array(
            'conditions' => array('Customer.user_id' => $this->Auth->user('id'))
        ));

        $this->set('customer', $customer);
        $this->set('status', "ok");
        $this->set('_serialize', array('customer', 'status'));
    }

	public function tickets() {

        $this->Paginator->settings['conditions']['Ticket.open'] = 1;
        //$this->Paginator->settings['limit'] = 1;
		$this->Ticket->recursive = 0;
		$this->set('tickets', $this->Paginator->paginate());

        $this->set('_serialize', array('tickets'));
	}

    public function tickets_archive() {
        $this->Paginator->settings['conditions']['Ticket.open'] = 0;
        $this->Ticket->recursive = 0;
        $this->set('tickets', $this->Paginator->paginate());
        $this->set('_serialize', array('tickets'));
    }

	public function ticket($id = null) {
		if (!$this->Ticket->exists($id)) {
            $this->set('status', 'error');
            $this->set('message', 'Invalid ticket');
		} else {

            $options = array('conditions' => array('Ticket.' . $this->Ticket->primaryKey => $id));
            $this->set('ticket', $this->Ticket->find('first', $options));

            // Mark all admin/mod messages as read (to remove it from notifications)
            $this->Ticket->Message->updateAll(array('Message.read' => 1), array('Message.ticket_id' => $id, 'Message.role !=' => 'customer'));

            $this->set('status', 'ok');
        }


        $this->set('_serialize', array('status', 'message', 'ticket'));
	}

	public function ticket_add() {
		if ($this->request->is('post')) {
			$this->Ticket->create();

            $customer = $this->Ticket->Customer->find('first', array(
                'conditions' => array('Customer.user_id' => $this->Auth->user('id'))
            ));

            $ticket = $this->request->data;

            $ticket['Ticket']['customer_id'] = $customer['Customer']['id'];

			if ($this->Ticket->save($ticket)) {

                $message = array(
                    'ticket_id' => $this->Ticket->id,
                    'body' => $ticket['Ticket']['message'],
                    'user_id' => $this->Auth->user('id'),
                    'full_name' => $customer['Customer']['full_name']
                );
                $this->Ticket->Message->save($message);

                $this->set('status', 'ok');
                $this->set('message', 'The ticket has been saved.');
			} else {
                $this->set('status', 'error');
                $this->set('message', 'The ticket could not be saved. Please, try again.');
			}
		}

        $this->set('_serialize', array('status', 'message'));
	}

    public function ticket_set_open() {

        $id = $this->request->data['Ticket']['id'];
        if (!$this->Ticket->exists($id)) {
            $this->set('status', array('error'));
            $this->set('message', array('Invalid ticket'));

        } else if ($this->request->is(array('post'))) {

            if ($this->Ticket->save($this->request->data)) {
                $this->set('status', 'ok');
                $this->set('message', 'The ticket has been saved.');
            } else {
                $this->set('status', 'error');
                $this->set('message','The ticket could not be saved. Please, try again.');
            }
        }

        $this->set('_serialize', array('status', 'message'));
    }

    public function ticket_set_satisfaction() {

        $id = $this->request->data['Ticket']['id'];
        if (!$this->Ticket->exists($id)) {
            $this->set('status', array('error'));
            $this->set('message', array('Invalid ticket'));

        } else if ($this->request->is(array('post'))) {

            if ($this->Ticket->save($this->request->data)) {
                $this->set('status', 'ok');
                $this->set('message', 'The ticket has been saved.');
            } else {
                $this->set('status', 'error');
                $this->set('message', 'The ticket could not be saved. Please, try again.');
            }
        }

        $this->set('_serialize', array('status', 'message'));
    }

    public function equipments() {
        $equipments = $this->Ticket->Equipment->find('all', array('recursive' => 0));
        $this->set(compact('equipments'));
        $this->set('_serialize', array('equipments'));
    }

    public function request_types() {
        $requestTypes = $this->Ticket->RequestType->find('all', array('recursive' => 0));
        $this->set(compact('requestTypes'));
        $this->set('_serialize', array('requestTypes'));
    }

    public function messages_unread() {

        // TODO filter by Message.user_id not Ticket.customer_id
        $customer = $this->Ticket->Customer->find('first', array(
            'conditions' => array('Customer.user_id' => $this->Auth->user('id'))
        ));

        $messages = $this->Ticket->Message->find('all', array('conditions' => array(
            'Message.role !=' => 'customer',
            'Ticket.customer_id' => $customer['Customer']['id'],
            'Message.read' => 0
        )));

        $this->set(compact('messages'));
        $this->set('_serialize', array('messages'));
    }

    public function message_add() {

        if ($this->request->is('post')) {

            $message = $this->request->data;

            if (!empty($message['Message']['ticket_id'])) {

                $customer = $this->Ticket->Customer->find('first', array(
                    'conditions' => array('Customer.user_id' => $this->Auth->user('id'))
                ));

                $exists = $this->Ticket->hasAny(array(
                    'Ticket.id' => $message['Message']['ticket_id'],
                    'Ticket.customer_id' => $customer['Customer']['id']
                ));

                if ($exists) {
                    $this->Ticket->Message->create();

                    $message['Message']['user_id'] = $this->Auth->user('id');
                    $message['Message']['full_name'] = $customer['Customer']['full_name'];
                    $message['Message']['role'] = 'customer';

                    if ($this->Ticket->Message->save($message)) {
                        $this->set('status', 'ok');
                        $this->set('message', 'The message has been saved.');
                    } else {
                        $this->set('status', 'error');
                        $this->set('message', 'The message could not be saved. Please, try again. 1');
                    }
                }

            } else {
                $this->set('status', 'error');
                $this->set('message', 'The message could not be saved. Please, try again. 2');
            }

            $this->set('_serialize', array('status', 'message'));

        }
    }
}
