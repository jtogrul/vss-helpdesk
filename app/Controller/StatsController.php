<?php
App::uses('AppController', 'Controller');
/**
 * Statuses Controller
 *
 * @property Status $Status
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class StatsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Toolbar');

    public $helpers = array(
        'Session',
        'Html' => array('className' => 'BoostCake.BoostCakeHtml'),
        'Form' => array('className' => 'BootstrapForm'),
        'Paginator' => array('className' => 'BoostCake.BoostCakePaginator'),
    );


    /**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {

        $this->loadModel('User');

        // Hack to use virtual field in containable
        $this->User->virtualFields['full_name'] = $this->User->Admin->virtualFields['full_name'];

        $admins = $this->User->find('list',
            array(
                'contain' => array('Admin'),
                'fields' => array('User.id', 'User.full_name'),
                'conditions' => array('User.role !=' => 'customer')
            )
        );

        $this->set(compact('admins'));
	}

    public function admin_equipments_by_month_ajax() {

        $this->loadModel('Ticket');

        $initial = $this->Ticket->find('all', array(
            'recursive' => 0,
            'fields' => array('YEAR(Ticket.created) as year', 'MONTH(Ticket.created) as month', 'Ticket.equipment_id', 'COUNT(Ticket.id) as count'),
            'group' => array('year', 'month', 'Ticket.equipment_id'),
            'order' => array('year' => 'asc', 'month' => 'asc')
        ));

        $equipments = $this->Ticket->Equipment->find('list');


        $stats = array();
        $stats['data'] = array();
        $data = array();

        foreach($initial as $row) {

            $key = $row['0']['year'].'-'.$row['0']['month'];
            $data[$key][$row['Ticket']['equipment_id']] = $row['0']['count'];
        }

        foreach($data as $key=>$value) {
            $value['m'] = $key;
            array_push($stats['data'], $value);
        }

        $stats['keys'] = array_keys($equipments);
        $stats['labels'] = array_values($equipments);

        return new CakeResponse(array('body'=> json_encode($stats),'status'=>200));

    }


    public function admin_answers_by_mods_ajax() {

        $this->loadModel('Message');

        $stats = null;

        $initial = $this->Message->find('all', array(
            'recursive' => 0,
            'fields' => array('DATE(Message.created) as date', 'COUNT(Message.id) as count'),
            'group' => array('date'),
            'order' => array('date' => 'asc')
        ));



        return new CakeResponse(array('body'=> json_encode($initial),'status'=>200));

    }

}
