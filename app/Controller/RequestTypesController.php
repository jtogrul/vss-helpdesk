<?php
App::uses('AppController', 'Controller');
/**
 * RequestTypes Controller
 *
 * @property RequestType $RequestType
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class RequestTypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Toolbar');

    public $helpers = array(
        'Session',
        'Html' => array('className' => 'BoostCake.BoostCakeHtml'),
        'Form' => array('className' => 'BootstrapForm'),
        'Paginator' => array('className' => 'BoostCake.BoostCakePaginator'),
    );


    /**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->RequestType->recursive = 0;
		$this->set('requestTypes', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->RequestType->exists($id)) {
			throw new NotFoundException(__('Invalid request type'));
		}
		$options = array('conditions' => array('RequestType.' . $this->RequestType->primaryKey => $id));
		$this->set('requestType', $this->RequestType->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RequestType->create();
			if ($this->RequestType->save($this->request->data)) {
				$this->Session->setFlash(__('The request type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The request type could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->RequestType->exists($id)) {
			throw new NotFoundException(__('Invalid request type'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RequestType->save($this->request->data)) {
				$this->Session->setFlash(__('The request type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The request type could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RequestType.' . $this->RequestType->primaryKey => $id));
			$this->request->data = $this->RequestType->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->RequestType->id = $id;
		if (!$this->RequestType->exists()) {
			throw new NotFoundException(__('Invalid request type'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RequestType->delete()) {
			$this->Session->setFlash(__('The request type has been deleted.'));
		} else {
			$this->Session->setFlash(__('The request type could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
