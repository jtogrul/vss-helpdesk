<?php
App::uses('AppController', 'Controller');
/**
 * Tickets Controller
 *
 * @property Ticket $Ticket
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TicketsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Toolbar');

    public $helpers = array(
        'Session',
        'Html' => array('className' => 'BoostCake.BoostCakeHtml'),
        'Form' => array('className' => 'BootstrapForm'),
        'Paginator' => array('className' => 'BoostCake.BoostCakePaginator'),
    );

/**
 * index method
 *
 * @return void
 */
	public function index() {
        	$is_archive = isset($this->request->query['archive']);

        	$this->Paginator->settings['conditions']['Ticket.open'] = $is_archive ? 0 : 1;
        	
        	$customer_id = $this->Session->read('Customer.Customer.id');
        	
        	$this->Paginator->settings['conditions']['Ticket.customer_id'] = $customer_id;

		$this->Ticket->recursive = 0;
		$this->set('tickets', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Ticket->exists($id)) {
			throw new NotFoundException(__('Invalid ticket'));
		}
		$options = array('conditions' => array('Ticket.' . $this->Ticket->primaryKey => $id));
		$this->set('ticket', $this->Ticket->find('first', $options));

        // Mark all admin/mod messages as read (to remove it from notifications)
        $this->Ticket->Message->updateAll(array('Message.read' => 1), array('Message.ticket_id' => $id, 'Message.role !=' => 'customer'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Ticket->create();

            $ticket = $this->request->data;

            $ticket['Ticket']['customer_id'] = $this->Session->read('Customer')['Customer']['id'];

			if ($this->Ticket->save($ticket)) {

                $message = array(
                    'ticket_id' => $this->Ticket->id,
                    'body' => $ticket['Ticket']['message'],
                    'user_id' => $this->Auth->user('id')
                );
                $this->Ticket->Message->save($message);

				$this->Session->setFlash(__('The ticket has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ticket could not be saved. Please, try again.'));
			}
		}
		$equipment = $this->Ticket->Equipment->find('list');
		$requestTypes = $this->Ticket->RequestType->find('list');
		$this->set(compact('equipment', 'requestTypes'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Ticket->exists($id)) {
			throw new NotFoundException(__('Invalid ticket'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Ticket->save($this->request->data)) {
				$this->Session->setFlash(__('The ticket has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ticket could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Ticket.' . $this->Ticket->primaryKey => $id));
			$this->request->data = $this->Ticket->find('first', $options);
		}
		$customers = $this->Ticket->Customer->find('list');
		$equipment = $this->Ticket->Equipment->find('list');
		$statuses = $this->Ticket->Status->find('list');
		$requestTypes = $this->Ticket->RequestType->find('list');
		$this->set(compact('customers', 'equipment', 'statuses', 'requestTypes'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Ticket->id = $id;
		if (!$this->Ticket->exists()) {
			throw new NotFoundException(__('Invalid ticket'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Ticket->delete()) {
			$this->Session->setFlash(__('The ticket has been deleted.'));
		} else {
			$this->Session->setFlash(__('The ticket could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {

        $is_archive = isset($this->request->query['archive']);

        $this->Paginator->settings['conditions']['Ticket.open'] = $is_archive ? 0 : 1;
        
        
        
        	if('moderator' == $this->Auth->user('role')) {
        	
        		$moderatorId = $this->Session->read('Auth.User.Admin.id');
        		$this->Paginator->settings['conditions']['Ticket.moderator_id'] = $moderatorId;
        	}

		$this->Ticket->recursive = 0;
		$this->set('tickets', $this->Paginator->paginate());
		
        		
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
	
		$this->loadModel('Admin');
	
		if (!$this->Ticket->exists($id)) {
			throw new NotFoundException(__('Invalid ticket'));
		}
		$options = array('conditions' => array('Ticket.id' => $id));
		$this->set('ticket', $this->Ticket->find('first', $options));

        	// Mark all customer messages as read (to remove it from notifications)
        	$this->Ticket->Message->updateAll(array('Message.read' => 1), array('Message.ticket_id' => $id, 'Message.role' => 'customer'));

        	$this->set('statuses', $this->Ticket->Status->find('list'));
        	$this->set('moderators', $this->Admin->find('list'));
        
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Ticket->create();
			if ($this->Ticket->save($this->request->data)) {
				$this->Session->setFlash(__('The ticket has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ticket could not be saved. Please, try again.'));
			}
		}
		$customer = $this->Ticket->Customer->find('list');
		$equipment = $this->Ticket->Equipment->find('list');
		$statuses = $this->Ticket->Status->find('list');
		$requestTypes = $this->Ticket->RequestType->find('list');
		$this->set(compact('customer', 'equipment', 'statuses', 'requestTypes'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Ticket->exists($id)) {
			throw new NotFoundException(__('Invalid ticket'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Ticket->save($this->request->data)) {
				$this->Session->setFlash(__('The ticket has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ticket could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Ticket.' . $this->Ticket->primaryKey => $id));
			$this->request->data = $this->Ticket->find('first', $options);
		}
		$customers = $this->Ticket->Customer->find('list');
		$equipment = $this->Ticket->Equipment->find('list');
		$statuses = $this->Ticket->Status->find('list');
		$requestTypes = $this->Ticket->RequestType->find('list');
		$this->set(compact('customers', 'equipment', 'statuses', 'requestTypes'));
	}

/**
v
 * * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Ticket->id = $id;
		if (!$this->Ticket->exists()) {
			throw new NotFoundException(__('Invalid ticket'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Ticket->delete()) {
			$this->Session->setFlash(__('The ticket has been deleted.'));
		} else {
			$this->Session->setFlash(__('The ticket could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function admin_status_set() {
        $id = $this->request->data['Ticket']['id'];
        if (!$this->Ticket->exists($id)) {
            throw new NotFoundException(__('Invalid ticket'));
        }
        if ($this->request->is(array('post'))) {
            if ($this->Ticket->save($this->request->data)) {
                $this->Session->setFlash(__('The ticket has been saved.'));

            } else {
                $this->Session->setFlash(__('The ticket could not be saved. Please, try again.'));
            }
        }

        return $this->redirect(array('action' => 'view', $id));
    }

    public function admin_open_set() {
        $id = $this->request->data['Ticket']['id'];
        if (!$this->Ticket->exists($id)) {
            throw new NotFoundException(__('Invalid ticket'));
        }
        if ($this->request->is(array('post'))) {
            if ($this->Ticket->save($this->request->data)) {
                $this->Session->setFlash(__('The ticket has been saved.'));

            } else {
                $this->Session->setFlash(__('The ticket could not be saved. Please, try again.'));
            }
        }

        return $this->redirect(array('action' => 'view', $id));
    }
    
    public function admin_moderator_set() {
        $id = $this->request->data['Ticket']['id'];
        if (!$this->Ticket->exists($id)) {
            throw new NotFoundException(__('Invalid ticket'));
        }
        if ($this->request->is(array('post'))) {
            if ($this->Ticket->save($this->request->data)) {
                $this->Session->setFlash(__('The ticket has been saved.'));

            } else {
                $this->Session->setFlash(__('The ticket could not be saved. Please, try again.'));
            }
        }

        return $this->redirect(array('action' => 'view', $id));
    }

    public function open_set() {

        // TODO auth check: belongs to user

        $id = $this->request->data['Ticket']['id'];
        if (!$this->Ticket->exists($id)) {
            throw new NotFoundException(__('Invalid ticket'));
        }
        if ($this->request->is(array('post'))) {
            if ($this->Ticket->save($this->request->data)) {
                $this->Session->setFlash(__('The ticket has been saved.'));

            } else {
                $this->Session->setFlash(__('The ticket could not be saved. Please, try again.'));
            }
        }

        return $this->redirect(array('action' => 'view', $id));
    }

    public function satisfied_set() {

        // TODO auth check: belongs to user

        $id = $this->request->data['Ticket']['id'];
        if (!$this->Ticket->exists($id)) {
            throw new NotFoundException(__('Invalid ticket'));
        }
        if ($this->request->is(array('post'))) {
            if ($this->Ticket->save($this->request->data)) {
                $this->Session->setFlash(__('The ticket has been saved.'));

            } else {
                $this->Session->setFlash(__('The ticket could not be saved. Please, try again.'));
            }
        }

        return $this->redirect(array('action' => 'view', $id));
    }
}
