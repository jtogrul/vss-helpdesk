/**
 * Created by togrul on 10/26/14.
 */

$(document).ready(function(){

    $("#messages-dropdown-toolbar").on("show.bs.dropdown", function() {
        $("#messages-box-toolbar").load(base_url + (user_role_general === 'admin'? 'admin/' : '') +'messages/unread_ajax');
    });
});