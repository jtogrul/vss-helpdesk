<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 * @property Admin $Admin
 * @property Customer $Customer
 */

App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {

    public $actsAs = array('Containable');
    public $displayField = 'username';

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasOne associations
 *
 * @var array
 */
	public $hasOne = array(
		'Admin',
		'Customer'
	);

    public function beforeSave($options = array())
    {
    
    	$old = $this->findById($this->data[$this->alias][$this->primaryKey]);
    	
        if (!$this->id || $old[$this->alias]['password'] != $this->data[$this->alias]['password'] ) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash($this->data[$this->alias]['password']);
        }

        if (empty($this->data['User']['role']))
        {
            $this->data['User']['role'] = 'customer';
        }
        return true;
    }

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Ticket' => array(
            'className' => 'Ticket',
            'foreignKey' => 'customer_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );


}
